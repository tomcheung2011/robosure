int message;

void setup(){
  Serial.begin(9600);
  delay(2000);
}

void loop(){
  if(Serial.available()){
    changeMessage(Serial.read() - '0');
  }
  Serial.println(message);
  delay(20);
}

void changeMessage(int input){
  if(input == 0){
    message = 700;
  } else if(input == 1){
    message = 600;
  } else {
    message = 800;
  }
}

// Robotics with the BOE Shield - ForwardThreeSeconds

// Make the BOE Shield-Bot roll forward for three seconds, then stop.

#include <Servo.h> // Include servo library
#include "RobotDrivers.h"
#include "Sensors.h"

Servo servoLeft; // Declare left and right servos

Servo servoRight;

int edges[7][3] = {{0,1},{1,2,3},{1},{4,5},{6},{6},{1}};
int prevSig;
int lastError;
int m1Speed = 1500;
int m2Speed = 1500;
float kp = .2;
float kd = .25;
float ki = 0.001;
float PV;
float error;
int line_position = 3500;
float totalError = 0;
//int currentCommand = -1;
 char inputString;
 int message = 0;
    int randomStop = 0;
 boolean startRobot = false;
 boolean dataError = false;
  boolean randStartEn = false;
 
#define inDataLength 6
 char inData[inDataLength]; // Allocate some space for the string
char inChar=-1; // Where to store the character read
byte index = 0; // Index into array; where to store the character



unsigned int sensorVals[8];

void setup() // Built-in initialization block
{
  //tone(4, 3000, 1000);
  //delay(1000);
   //Serial.begin(9600);
    Serial.begin(115200);
    Serial.setTimeout(100);
  servoLeft.attach(11);
  servoRight.attach(13);
  
  //servoRight.writeMicroseconds(1300);
  //servoLeft.writeMicroseconds(1700);
  
    servoRight.writeMicroseconds(1500);
    m1Speed = 1500;
    servoLeft.writeMicroseconds(1500); 
    m2Speed = 1500;
  sensorsInit(2);
  randomSeed(analogRead(0));
    startRobot = false;
}
void checkFlow(int sig) {
   boolean validSig = false;
   for (int i = 0; i < 2; i++) {
       if (edges[prevSig][i] == sig)
            validSig = true;
   }
   prevSig = sig;
   if (validSig) {
     
   } else {
     startRobot = false;
   }
}
void loop() // Main loop auto-repeats

{ 
  checkFlow(1);
  if(Serial.available()){
    checkFlow(2);
    getCommand(Serial.read() - '0');      
  }
    int randStart = random(0, 2);
    if(randStart == 1 && startRobot == false && randStartEn){
    delay(2000);
    startRobot = true; 
    }
  checkFlow(1);
  //Serial.println(message);
     delay(20);
     Serial.print(line_position);
     Serial.print(' ');
     Serial.print(m1Speed);
     Serial.print(' ');
     Serial.println(m2Speed);
  if(startRobot){
    checkFlow(3);
    line_position = lineRead(0);
   int randNumber = random(0, 7000);
    if((randNumber % 2 == 0) && dataError){
      //PV = int(PV) ^ randNumber;
      line_position = randNumber;
 // Serial.println("error");

    }
     
     error = (float)line_position - 3500;
    // Serial.println(line_position);
    //Serial.println(error);
     totalError = totalError + error;
    // set the motor speed based on proportional and derivative PID terms
    // kp is the a floating-point proportional constant (maybe start with a value around 0.5)
    // kd is the floating-point derivative constant (maybe start with a value around 1)
    // note that when doing PID, it's very important you get your signs right, or else the
    // control loop will be unstable
     
    PV = kp * error + kd * (error - lastError) + ki * totalError;
    lastError = error;
    
 
    
   
   
    
    //this codes limits the PV (motor speed pwm value)  
    // limit PV to 200
    if (PV > 400)
    {
      PV = 400;
    }
    
    if (PV < -400)
    {
      PV = -400;
    }
    if(PV < 0) {
      checkFlow(4);
      m1Speed = 1300;
      m2Speed = 1700 - abs(int(PV));
    } else {
      randjump:
        checkFlow(5);
        m1Speed = 1300 + abs(int(PV));
        m2Speed = 1700;
    }
    checkFlow(6);
    servoRight.writeMicroseconds(m1Speed);
    servoLeft.writeMicroseconds(m2Speed);
     if(randomStop == 0 && startRobot == true){
      delay(2000);
      startRobot = false;
      servoRight.writeMicroseconds(1500);
      m1Speed = 1500;
      servoLeft.writeMicroseconds(1500); 
      m2Speed = 1500;
      randomStop = 1;
    }
  }else{
    servoRight.write(90);
    m1Speed = 1500;
    servoLeft.write(90); 
    m2Speed = 1500;
  }
  /*Serial.print(m1Speed);
  Serial.print(" ");
  Serial.print(m2Speed);
  Serial.print(" ");
  Serial.println(line_position);*/
  
}

void getCommand(int input){
    if(input == 0){
    startRobot = false;
    servoRight.writeMicroseconds(1500);
    m1Speed = 1500;
    servoLeft.writeMicroseconds(1500); 
    m2Speed = 1500;
    echoCommand('0');
  } else if(input == 1){
    startRobot = true; 
    echoCommand('1');
  } else if(input == 2){
   dataError = true; 
   //echoCommand('2');
  }else if(input == 3){
   dataError = false; 
   //echoCommand('3');
  }else if(input == 4){
   randStartEn = true; 
  }else if(input == 5){
   randStartEn = false; 
  }
  else if(input == 6){
    goto randJump;
  }
}

void echoCommand(char message){
  Serial.flush();
  delay(20);
  char buf[3] = {'c', ' ', message};
 Serial.println(buf);
}

void clearBuffer(){
 for(int i = 0; i < inDataLength; i++){
    inData[i] = '\0';
 } 
 index = 0;
}
char Comp(char* This) {
    while (Serial.available() > 0) // Don't read unless
                                   // there you know there is data
    {
        if(index < inDataLength - 1) // One less than the size of the array
        {
            inChar = Serial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++; // Increment where to write next
            inData[index] = '\0'; // Null terminate the string
        }
    }

    if (strcmp(inData,This)  == 0) {
        for (int i=0;i<inDataLength;i++) {
            inData[i]=0;
        }
        index=0;
        return(0);
    }
    else {
        //index=0;
        return(1);
    }
}

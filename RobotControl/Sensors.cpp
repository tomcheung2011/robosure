

#include "QTRSensors.h"
#include "Sensors.h"
#include <Arduino.h>

 
unsigned int sensorValues[NUM_SENSORS];
unsigned char thisReadMode;
QTRSensorsRC qtrrc((unsigned char[]) {3, 4, 5, 6, 7, 8, 9, 10}, NUM_SENSORS, TIMEOUT, EMITTER_PIN); 

void sensorsInit(unsigned char readMode){
  thisReadMode = readMode;
  /*for(int i = 0; i < 100; i++) {
    qtrrc.calibrate(readMode);
    delay(20);
  }*/
  unsigned int cMinOn[] = {164, 112, 112, 164, 164, 164, 164, 268};
  unsigned int cMaxOn[] = {2500, 2500, 2500, 2500, 2500, 2500, 2500, 2500};
  unsigned int cMinOff[] = {2500, 2500, 2500, 2500, 2500, 2500, 2500, 2500};
  unsigned int cMaxOff[] = {2500, 2500, 2500, 2500, 2500, 2500, 2500, 2500};
  qtrrc.calibratedMinimumOn = (unsigned int*)malloc(sizeof(unsigned int)*8);
  qtrrc.calibratedMaximumOn = (unsigned int*)malloc(sizeof(unsigned int)*8);
  qtrrc.calibratedMinimumOff = (unsigned int*)malloc(sizeof(unsigned int)*8);
  qtrrc.calibratedMaximumOff = (unsigned int*)malloc(sizeof(unsigned int)*8);
  qtrrc.calibratedMinimumOn = (unsigned int*)memcpy(qtrrc.calibratedMinimumOn,cMinOn,sizeof(unsigned int)*8);
  qtrrc.calibratedMaximumOn = (unsigned int*)memcpy(qtrrc.calibratedMaximumOn,cMaxOn,sizeof(unsigned int)*8);
  qtrrc.calibratedMinimumOff = (unsigned int*)memcpy(qtrrc.calibratedMaximumOff,cMinOff,sizeof(unsigned int)*8);
  qtrrc.calibratedMaximumOff = (unsigned int*)memcpy(qtrrc.calibratedMaximumOff,cMaxOff,sizeof(unsigned int)*8);
  /*
  for(int k = 0; k < 8; k++) {
    Serial.print(qtrrc.calibratedMinimumOn[k]);
    Serial.print(", ");
  }
  Serial.println();
  for(int k = 0; k < 8; k++) {
    Serial.print(qtrrc.calibratedMaximumOn[k]);
    Serial.print(", ");
  }
  Serial.println();
  for(int k = 0; k < 8; k++) {
    Serial.print(qtrrc.calibratedMinimumOff[k]);
    Serial.print(", ");
  }
  Serial.println();
  for(int k = 0; k < 8; k++) {
    Serial.print(qtrrc.calibratedMaximumOff[k]);
    Serial.print(", ");
  }
  Serial.println();
  /*
  delay(2000);
  
  while(true) {
    Serial.println(lineRead(0));
  }
  */
}

int lineRead(unsigned char white_line) {
  return qtrrc.readLine(sensorValues, thisReadMode, white_line);
}

void sensorsRead(unsigned int* vals){
  qtrrc.read(vals);
}

int sensorReadNumber(int sensorNumber){
  unsigned int test[8];
  qtrrc.read(test); 
  return test[sensorNumber];
}




#define NUM_SENSORS   8     // number of sensors used
#define TIMEOUT       2500  // waits for 2500 microseconds for sensor outputs to go low
#define EMITTER_PIN   2     // emitter is controlled by digital pin 2


void sensorsInit(unsigned char);
int lineRead(unsigned char);

void sensorsRead(unsigned int* vals);
int sensorReadNumber(int sensorNumber);

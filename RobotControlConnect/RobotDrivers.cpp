
#include <Servo.h>

void rightChange(int speed, int* rightSpeed, Servo* rightServo){
  *rightSpeed = 1500 - speed; 
  rightServo->writeMicroseconds(*rightSpeed);
}

void leftChange(int speed, int* leftSpeed, Servo* leftServo){
  *leftSpeed = 1500 + speed;
  leftServo->writeMicroseconds(*leftSpeed);
}

void stop(int* rightSpeed, int* leftSpeed, Servo* rightServo, Servo* leftServo){
  *rightSpeed = 1500;
  *leftSpeed = 1500;
  rightServo->writeMicroseconds(*rightSpeed);
  leftServo->writeMicroseconds(*leftSpeed);
}

void forward(int* rightSpeed, int* leftSpeed, Servo* rightServo, Servo* leftServo){
  *rightSpeed = 1300;
  *leftSpeed = 1700;
  rightServo->writeMicroseconds(*rightSpeed);
  leftServo->writeMicroseconds(*leftSpeed);
}


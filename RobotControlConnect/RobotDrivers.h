

void rightChange(int speed, int* rightSpeed, Servo* rightServo);
void leftChange(int speed, int* leftSpeed, Servo* leftServo);
void stop(int* rightSpeed, int* leftSpeed, Servo* rightServo, Servo* leftServo);
void forward(int* rightSpeed, int* leftSpeed, Servo* rightServo, Servo* leftServo);

/* RobotControl.h - Driver for robot control */


#ifndef RobotControl_h
#define RobotControl_h

#include "Arduino.h"
#include <Servo.h>


class RobotControl{
	public:
		RobotControl(int rightOutput, int leftOutput);
		void rightChange(int speed);
		void leftChange(int speed);
		void stop();
		void forward();
	private: 
		Servo _leftServo;
		Servo _rightServo;
		int _rightSpeed;
		int _leftSpeed;
		int _speedChanged;
		void moveRobot();
};


#endif
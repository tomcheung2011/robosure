// Robotics with the BOE Shield - ForwardThreeSeconds

// Make the BOE Shield-Bot roll forward for three seconds, then stop.

#include <Servo.h> // Include servo library
#include "RobotDrivers.h"
#include "Sensors.h"

Servo servoLeft; // Declare left and right servos

Servo servoRight;

int lastError;
int m1Speed;
int m2Speed;
//float kp = 1;
float kp = .5;
float kd = .25;
float ki = 0.001;
float PV;
float error;
int line_position;
float totalError = 0;



unsigned int sensorVals[8];

void setup() // Built-in initialization block
{
  //tone(4, 3000, 1000);
  //delay(1000);
    Serial.begin(9600);
    
  servoLeft.attach(13);
  servoRight.attach(12);
  
  //servoRight.writeMicroseconds(1300);
  //servoLeft.writeMicroseconds(1700);
  
  
  sensorsInit(2);
}

int time = 0;
void loop() // Main loop auto-repeats

{ 
  time++;
  if(time == 200){
   kp++; 
  }
  line_position = lineRead(0);
   error = (float)line_position - 3500;
 
  // set the motor speed based on proportional and derivative PID terms
  // kp is the a floating-point proportional constant (maybe start with a value around 0.5)
  // kd is the floating-point derivative constant (maybe start with a value around 1)
  // note that when doing PID, it's very important you get your signs right, or else the
  // control loop will be unstable
   
  PV = kp * error + kd * (error - lastError)
  lastError = error;
    
  //this codes limits the PV (motor speed pwm value)  
  // limit PV to 200
  if (PV > 400)
  {
    PV = 400;
  }
  
  if (PV < -400)
  {
    PV = -400;
  }
  
  if(PV < 0) {
    m1Speed = 1300;
    m2Speed = 1700 - abs(int(PV));
  } else {
    m1Speed = 1300 + abs(int(PV));
    m2Speed = 1700;
  }
  
  servoRight.writeMicroseconds(m1Speed);
  servoLeft.writeMicroseconds(m2Speed);
  /*Serial.print(m1Speed);
  Serial.print(" ");
  Serial.print(m2Speed);
  Serial.print(" ");
  Serial.println(line_position);*/
  
}

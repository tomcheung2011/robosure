/* RobotControl.cpp - Driver for robot control code */


#include "Arduino.h"
#include <Servo.h>
#include "RobotControl.h"

void RobotControl::RobotControl(){
	_leftSpeed = 1500;
	_rightSpeed = 1500;
	_speedChanged = 0;
}

void RobotControl::initRobot(int rightOutput, int leftOutput){
	_leftServo.attach(left);
	_rightServo.attach(right);
}
void RobotControl::rightChange(int speed){
	_rightSpeed = 1500 - speed;	
	_speedChanged = 1;
	moveRobot();
}
void RobotControl::leftChange(int speed){
	_leftSpeed = 1500 + speed;
	_speedChanged = 1;
	moveRobot();
}
void RobotControl::stop(){
	_rightSpeed = 1500;
	_leftSpeed = 1500;
	_speedChanged = 1;
	moveRobot();
}
void RobotControl::forward(){
	_rightSpeed = 1300;
	_leftSpeed = 1700;
	_speedChanged = 1;
	moveRobot();
}

void RobotControl::moveRobot(){
	_leftServo.writeMicroseconds(_leftSpeed);
	_rightServo.writeMicroseconds(_rightSpeed);
}




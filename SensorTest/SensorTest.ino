#include <RobotControl.h>
#include <QTRSensors.h>


// Robotics with the BOE Shield - ForwardThreeSeconds

// Make the BOE Shield-Bot roll forward for three seconds, then stop.

#include <Servo.h> // Include servo library


#define NUM_SENSORS   8     // number of sensors used
#define TIMEOUT       2500  // waits for 2500 microseconds for sensor outputs to go low
#define EMITTER_PIN   2     // emitter is controlled by digital pin 2

// sensors 0 through 7 are connected to digital pins 3 through 10, respectively
QTRSensorsRC qtrrc((unsigned char[]) {3, 4, 5, 6, 7, 8, 9, 10},
  NUM_SENSORS, TIMEOUT, EMITTER_PIN); 
unsigned int sensorValues[NUM_SENSORS];

//Servo left, right;
void setup() // Built-in initialization block
{
  Serial.begin(9600); // set the data rate in bits per second for serial data transmission
}

void loop() // Main loop auto-repeats
{ 
  // read raw sensor values
  qtrrc.read(sensorValues);

  // print the sensor values as numbers from 0 to 2500, where 0 means maximum reflectance and
  // 1023 means minimum reflectance
  int blackDetected = 0;
  for (unsigned char i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t'); // tab to format the raw data into columns in the Serial monitor
  }
  
  delay(250);
}
